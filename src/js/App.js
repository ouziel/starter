import React, {PropTypes} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {Link} from 'react-router';
const App = ({children, location}) => {
	return (
		<div>
			<ul>
				<li>
					<Link to="/page1">Pageee 1</Link>
				</li>
				<li>
					<Link to="/page2">Page 2</Link>
				</li>
			</ul>
			<ReactCSSTransitionGroup component="div" transitionName="fade" transitionEnterTimeout={500} transitionLeaveTimeout={500}>
				{React.cloneElement(children, {key: location.pathname})}
			</ReactCSSTransitionGroup>
		</div>
	)
}
App.propTypes = {};
export default App;