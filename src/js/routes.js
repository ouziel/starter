import React, {PropTypes} from 'react';
import ReactDOM, {render} from 'react-dom'
import {browserHistory, Router, Route, IndexRoute, Link} from 'react-router'
import App from './App';
import {Page1, Page2, Index} from './pages';

const log = () => {
    console.log('entered!');
}
const routes = {
    path: '/',
    component: App,
    indexRoute: {
        component: Index
    },
    childRoutes: [
        {
            path: 'page1',
            component: Page1,
						onEnter: log
        }, {
            path: 'page2',
            component: Page2
        }
    ]
}

render((<Router history={browserHistory} routes={routes}/>), document.getElementById('root'));